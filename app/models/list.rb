class List < ApplicationRecord
    belongs_to :user

    acts_as_list

    has_many :cards, -> { order(position: :asc) }, dependent: :destroy
  
    validates :name, presence: true
end
