json.extract! list, :id, :user, :name, :position, :created_at, :updated_at
json.url list_url(list, format: :json)
