# TASK MANAGER

# Sobre o projeto
Este projeto foi feito como o terceiro trabalho da disciplina Orientação a Objetos que tinha como objetivo desenvolver uma aplicação web utilizando, principalmente, a linguagem Ruby e a gem Rails.
## Requisitos do sistema
- Ruby version 2.6.5 p114
## Para rodar o programa
1. Clone o repositório
2. Abra a pasta no seu terminal
3. Rode os seguintes comandos
``` ruby
bundle install
yarn install --check-files
rails db:migrate
rails s
```
4. Abra o seu browser no endereço [localhost:3000](localhost:3000)

# Sobre a aplicação
A aplicação foi desenvolvida com o objetivo de ajudar pessoas com dificuldades em
se organizar. Consiste em um site que organiza suas tarefas em listas e cartões.
O usuário poderá criar varias listas e adicionar a elas seus cartões. Cada cartão poderá ser movido para qualquer outra lista já criada. O usuário também pode editar um cartão já criado assim também como excluí-lo. O mesmo vale para as listas.

# Funcionalidades
A aplicação possui as funcionalidades abaixo, além da funcionalidade de login, logout e register.
- Criação/Remoção de Listas
- Criação/Remoção de Cartões
- Mover Cartões para outras Listas