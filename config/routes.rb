Rails.application.routes.draw do
    devise_for :users, :controllers => { registrations: 'registrations' }

    get '/', to: 'home#index'

    resources :lists do
      member do
        patch :move
      end
    end
    
    resources :cards do
      member do
        patch :move
      end
    end
  
    devise_scope :user do
      root :to => 'devise/sessions#new'
    end
end
